module github.com/kamalshkeir/ksbus

go 1.20

require (
	github.com/kamalshkeir/klog v1.0.0
	github.com/kamalshkeir/kmap v1.1.1
	github.com/kamalshkeir/kmux v1.9.4
)

require (
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
